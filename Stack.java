package Lab3;

public class Stack {
	int size;
	Node top;
	public Stack()
	{
		top = null;
		size = 0;
	}
	public void push(int a)
	{
		Node newNode = top;
		top = new Node();
		top.item = a;
		top.next = newNode;
		size++;
	}
	public boolean isEmpty()
	{
		return top == null;
	}
	public int pop(){
		if(!isEmpty())
		{
			int item = top.item;
			top = top.next;
			size--;
			return item;
		}
		else
			return -1;	
	}
	public int numOfElements()
	{
		return size;
	}
	public int search(int a)
	{
		int i = 1;
		while(!isEmpty())
		{
			if(top.item == a)
				return i;
			i++;
			top = top.next;
		}
		return 0;
	}
	public void display()
	{
		while(!isEmpty())
		{
			System.out.println(top.item + " ");
			top = top.next;
		}
	}
}
